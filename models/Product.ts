export interface Product {
    sku: string
    name: string
    brand: string
    size: string
    price: number
    principalImage: string
    otherImages: string[]
}
