/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  // rewrites: [
  //   {
  //     source: '/api/v1/:path*',
  //     destination: 'http://localhost:8080/:path*'
  //   }
  // ],
  images: {
    domains: ['falabella.scene7.com'],
  },
}

module.exports = nextConfig
