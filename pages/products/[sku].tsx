import { GetServerSideProps, NextPage } from "next"
import Head from "next/head"
import ProductForm from "../../components/ProductForm"
import { Product } from "../../models/Product"
import client from "../../services/client"
import styles from '../../styles/Home.module.css'

interface Props {
  product: Product
}

export const getServerSideProps: GetServerSideProps<Props> = async (context) => {
  const { sku } = context.query

  const product = await client.getOneProduct(sku as string)

  return {
    notFound: product.sku == null,
    props: {
      product,
    },
  }
}

const ProductDetails: NextPage<Props> = ({ product }) => {
  return (
    <div className={styles.container}>
      <Head>
        <title>Products - {product.name}</title>
      </Head>

      <main className={styles.main}>
        <h1 className={styles.title}>
          Products
        </h1>

        <h3 className={styles.description}>
          {product.name}
        </h3>

        <ProductForm product={product} />
      </main>

      <footer className={styles.footer}>
        <a
          href="https://gitlab.com/users/nLeporati/projects"
          target="_blank"
          rel="noopener noreferrer"
        >
          Nicolás Leporati
        </a>
      </footer>
    </div>
  )
}

export default ProductDetails
