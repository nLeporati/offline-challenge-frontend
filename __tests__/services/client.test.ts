import '@testing-library/jest-dom'
import { Product } from '../../models/Product'
import client from '../../services/client'

const mockProduct: Product = {
    name: "test",
    brand: "brand",
    sku: "1",
    size: "31",
    price: 1000,
    principalImage: "http://ima.ge",
    otherImages: []
}

describe('client', () => {
    it('getOneProduct', async () => {
        jest.spyOn(window, "fetch").mockImplementationOnce(jest.fn(() => Promise.resolve(
            { json: () => Promise.resolve(mockProduct)}
        )) as jest.Mock)
        const product = await client.getOneProduct('1')
        expect(product).toEqual(mockProduct)
    })

    it('getProducts', async () => {
        jest.spyOn(window, "fetch").mockImplementationOnce(jest.fn(() => Promise.resolve(
            { json: () => Promise.resolve({ products: [{ name: 'test'}] })}
        )) as jest.Mock)
        const list = await client.getProducts()
        expect(list.products.length).toBe(1)
    })

    it('updateProduct', async () => {
        jest.spyOn(window, "fetch").mockImplementationOnce(jest.fn(() => Promise.resolve(
            { json: () => Promise.resolve( { name: 'test'} )}
        )) as jest.Mock)
        const product = await client.updateProduct(mockProduct)
        expect(product.name).toBe('test')
    })
})