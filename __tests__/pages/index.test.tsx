import '@testing-library/jest-dom'
import { cleanup, fireEvent, render, screen } from '@testing-library/react'
import { GetServerSidePropsContext } from 'next'
import { ParsedUrlQuery } from 'querystring'
import { Product } from '../../models/Product'
import Home, { getServerSideProps } from '../../pages'
import client from '../../services/client'

const mockProduct: Product = {
    name: "test",
    brand: "brand",
    sku: "1",
    size: "31",
    price: 1000,
    principalImage: "http://ima.ge",
    otherImages: []
}

describe('Home', () => {
    
    beforeEach(() => {
        cleanup()
        jest.clearAllMocks()
    })

    it("get product ok", async () => {
        const clientSpy = jest.spyOn(client, 'getProducts');
        const resp = { products: [mockProduct] }
        clientSpy.mockImplementationOnce(() => Promise.resolve(resp))

        const value = await getServerSideProps({} as GetServerSidePropsContext);
        expect(value).toEqual({props: resp});
    });

    it('renders a heading', () => {
        const useRouter = jest.spyOn(require('next/router'), 'useRouter');

        useRouter.mockImplementationOnce(() => ({
            push: jest.fn(),
        }));

        render(<Home products={[mockProduct]} />)

        const heading = screen.getByRole('heading', {
            name: /Welcome to Offline Challenge!/i,
        })

        const card = screen.getByRole('heading', {
            name: /\$1000/i,
        })

        fireEvent.click(card)
        expect(heading).toBeTruthy()
        expect(useRouter).toBeCalledTimes(1)
    })
})
