import '@testing-library/jest-dom'
import { Product } from '../../../models/Product'
import { cleanup, fireEvent, render, screen } from '@testing-library/react'
import ProductDetails, { getServerSideProps } from '../../../pages/products/[sku]'
import client from '../../../services/client'
import { ParsedUrlQuery } from 'querystring'
import { GetServerSidePropsContext, GetServerSidePropsResult } from 'next'

const mockProduct: Product = {
    name: "test",
    brand: "brand",
    sku: "1",
    size: "31",
    price: 1000,
    principalImage: "http://ima.ge",
    otherImages: []
}

describe('ProductDetails', () => {

    beforeEach(() => {
        cleanup()
        jest.clearAllMocks()
    })

    it("get product ok", async () => {
        const clientSpy = jest.spyOn(client, 'getOneProduct');
        clientSpy.mockImplementationOnce(() => Promise.resolve(mockProduct))

        const context = {
            query: { sku: "1" } as ParsedUrlQuery
        }
        const value = await getServerSideProps(context as GetServerSidePropsContext);
        expect(value).toEqual({notFound: false, props: {product: mockProduct}});
    });

    it('edit product ok', async () => {
        const clientSpy = jest.spyOn(client, 'updateProduct');
        const routerSpy = jest.spyOn(require('next/router'), 'useRouter');
        
        clientSpy.mockImplementationOnce(() => Promise.resolve(mockProduct))
        routerSpy.mockImplementationOnce(() => ({ replace: jest.fn }))

        render(<ProductDetails product={mockProduct} />)

        screen.getByRole('heading', { name: /test/i })
        const form = screen.getByRole('form')

        fireEvent.submit(form)
        expect(routerSpy).toHaveBeenCalled()
    })

    it("get product not found", async () => {
        const clientSpy = jest.spyOn(client, 'getOneProduct');
        clientSpy.mockImplementationOnce(() => Promise.resolve({}))

        const context = {
            query: { sku: "1" } as ParsedUrlQuery
        }
        const value = await getServerSideProps(context as GetServerSidePropsContext);
        expect(value).toEqual({notFound: true, props: {product: {}}});
    });

    it('edit product error', async () => {
        const clientSpy = jest.spyOn(client, 'updateProduct');
        const routerSpy = jest.spyOn(require('next/router'), 'useRouter');
        
        // @ts-ignore
        clientSpy.mockImplementationOnce(() => Promise.resolve({}))
        routerSpy.mockImplementationOnce(() => ({ replace: jest.fn }))

        render(<ProductDetails product={mockProduct} />)

        screen.getByRole('heading', { name: /test/i })
        const form = screen.getByRole('form')

        fireEvent.submit(form)
    })
})

