import { Product } from "../models/Product";
import { ProductList } from "../models/ProductList";

const HOST = 'http://localhost:8080/api/v1'
const HEADERS = { 'Content-Type': 'application/json' }

async function getOneProduct(sku: String) {
    const endpoint = `${HOST}/products/${sku}`
    const response = await fetch(endpoint, {method: 'GET', headers: HEADERS})
    return await response.json()
}

async function getProducts(): Promise<ProductList> {
    const endpoint = `${HOST}/products`
    const response = await fetch(endpoint, {method: 'GET', headers: HEADERS})
    return await response.json()
}

async function updateProduct(product: Product): Promise<Product> {
    const endpoint = `${HOST}/products/${product.sku}`

    const response = await fetch(endpoint, {
        method: 'POST',
        headers: HEADERS,
        body: JSON.stringify(product),
    })

    return await response.json()
}

export default {
    getProducts,
    getOneProduct,
    updateProduct
}