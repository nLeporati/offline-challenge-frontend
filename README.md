# Offline challenge

## Architecture

The NextJs structure is followed, which offers us pre-defined conventions and functionalities on the fly.

As an addition, only the `services` folder is added for communication utilities with the backend.

Unit tests are performed with `jest` and `react-testing-library` on the `__tests__` folder. Integration tests e2e are omitted.

## Requirements

*This project uses pnpm, to use another tool first remove pnpm-lock.yaml and reinstall with you desired one.*

- Node

## Getting Started

First, run the backend server to have access to the services

Then, run the development server:

```bash
pnpm run dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

To check the unit tests run:
```bash
pnpm run test
```

## Learn More

To learn more about Next.js, take a look at the following resources:

- [Next.js Documentation](https://nextjs.org/docs) - learn about Next.js features and API.
- [Learn Next.js](https://nextjs.org/learn) - an interactive Next.js tutorial.

This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

You can check out [the Next.js GitHub repository](https://github.com/vercel/next.js/) - your feedback and contributions are welcome!
