import { FC, FormEvent } from 'react';
import { useRouter } from 'next/router';
import { Product } from '../models/Product';
import client from '../services/client';

interface Props {
  product: Product
}

const ProductForm: FC<Props> = ({ product }) => {
  const router = useRouter()

  const handleSubmit = async (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault()
    const request: any = event.currentTarget.elements

    const data: Product = {
      sku: request.sku.value,
      name: request.name.value,
      brand: request.brand.value,
      size: request.size.value,
      price: Number(request.price.value),
      principalImage: request.image.value,
      otherImages: []
    }

    const result = await client.updateProduct(data)

    if (result.name != null) {
      router.replace("/")
    } else {
      window.alert(`Something bad happens :(`)
    }
  }

  return (
    <form onSubmit={handleSubmit} role="form">
      <p>
        <label htmlFor="sku">SKU:</label>
        <input
          required
          type="text"
          id="sku"
          name="sku"
          defaultValue={product.sku}
          pattern="FAL-[1-9][0-9]{6,7}" />
      </p>
      <p>
        <label htmlFor="name">Name:</label>
        <input
          required
          type="text"
          id="name"
          name="first"
          defaultValue={product.name}
          minLength={3}
          maxLength={50} />
      </p>
      <p>
        <label htmlFor="brand">Brand:</label>
        <input
          required
          type="text"
          id="brand"
          name="brand"
          defaultValue={product.brand}
          minLength={3}
          maxLength={50} />
      </p>
      <p>
        <label htmlFor="size">Size:</label>
        <input
          type="text"
          id="size"
          name="size"
          defaultValue={product.size}
          minLength={1} />
      </p>
      <p>
        <label htmlFor="price">Price:</label>
        <input
          required
          type="number"
          id="price"
          name="price"
          defaultValue={product.price}
          min={1.00}
          max={99999999.00} />
      </p>
      <p>
        <label htmlFor="image">Image:</label>
        <input
          required
          type="string"
          id="image"
          name="image"
          defaultValue={product.principalImage} />
      </p>
      <button type="submit">Submit</button>
    </form>
  )
}

export default ProductForm
